import React, { Component } from 'react'

class Quest extends Component {
  render() {
    return (
      <div>
        <div>
          {this.props.quest.title}: {this.props.quest.description}
        </div>
      </div>
    )
  }
}

export default Quest