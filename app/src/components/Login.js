import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { AUTH_TOKEN } from '../constants'

const LOGIN_SHOWCASE_PLAYER = gql`
  {
    showcaseToken
  }
`

const LOGIN_SHOWCASE_DM = gql`
  {
    showcaseToken(isDm:true)
  }
`


class Login extends Component {

  render() {
    let tokenRequest = this.props.location.pathname.search('dm') > 0 ? LOGIN_SHOWCASE_DM : LOGIN_SHOWCASE_PLAYER
    return (
      <div>
        <h4 className="mv3">Login as showcase player</h4>
        <div className="flex mt3">
          <Query
            query={tokenRequest}
            onCompleted={data => this._confirm(data)}
          >
            {({ loading, error, data }) => {
              if (loading) return <div>Fetching</div>
              if (error) return <div>Error</div>
              return (
                <div>
                  ok
                </div>
              )
            }}
          </Query>
        </div>
      </div>
    )
  }

  _confirm = async data => {
    const token = data.showcaseToken
    this._saveUserData(token)
    this.props.history.push(`/`)
  }

  _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token)
  }
}

export default Login