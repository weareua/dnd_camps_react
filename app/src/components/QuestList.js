import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Quest from './Quest'

const CAMPAIGN_QUESTS_QUERY = gql`
  { 
    campaignQuestsAll{
      id
      title
      description
    }
  }
`

class QuestList extends Component {
  render() {
    return (
      <Query query={CAMPAIGN_QUESTS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>
          const questsToRender = data.campaignQuestsAll
    
          return (
            <div>
              {questsToRender.map(quest => <Quest key={quest.id} quest={quest} />)}
            </div>
          )
        }}
      </Query>
    )
  }
}

export default QuestList