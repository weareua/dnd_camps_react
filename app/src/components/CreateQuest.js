import React, { Component } from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'


const CAMPAIGN_QUEST = gql`
  mutation CampaignQuest($description: String!, $title: String!){
    campaignQuest(input: {title: $title, description: $description, questType: A}) {
      quest {
        id
        title
        description
        questType
        createdDate
      }
    }
  }

`


class CreateQuest extends Component {
  state = {
    description: '',
    title: '',
  }

  render() {
    const { description, title } = this.state
    return (
      <div>
        <div className="flex flex-column mt3">
          <input
            className="mb2"
            value={title}
            onChange={e => this.setState({ title: e.target.value })}
            type="text"
            placeholder="Quest title"
          />
          <input
            className="mb2"
            value={description}
            onChange={e => this.setState({ description: e.target.value })}
            type="text"
            placeholder="Quest description"
          />
        </div>
        <Mutation
          mutation={CAMPAIGN_QUEST}
          variables={{ description, title }}
          onCompleted={() => this.props.history.push('/')}>
            {campaignQuest => <button onClick={campaignQuest}>Submit</button>}
        </Mutation>
      </div>
    )
  }
}

export default CreateQuest