import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import QuestList from './QuestList'
import CreateQuest from './CreateQuest'
import Header from './Header'
import Login from './Login'


class App extends Component {
  render() {
    return (
    <div className="center w85">
      <Header />
      <div className="ph3 pv1 background-gray">
        <Switch>
          <Route exact path="/" component={QuestList} />
          <Route exact path="/create" component={CreateQuest} />
          <Route exact path="/showcase-player" component={Login} />
          <Route exact path="/showcase-dm" component={Login} />
        </Switch>
      </div>
    </div>
  )
  }
}

export default App